<?php

    include_once '../config/dbConfig.php';

    function  selectAllUsers($conn , $email)
    {
        $result;
        $query = "SELECT * FROM user;";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $query)) {
            header("location: ../index.php?error=selectAllError");
            exit();
        }
        mysqli_stmt_execute($stmt);
        $resultData = mysqli_stmt_get_result($stmt);

        if ($row = mysqli_fetch_assoc($resultData)) {
            return $row;
        } else {
            $result = false;
        }
        return $result;
        
        mysqli_stmt_close($stmt);
        
    }

    function updateAnAccount($conn, $name, $mail, $email, $role)
    {
        $query = "UPDATE user SET name = ?, email = ? where email = ?;";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $query)) {
            header("location: ../index.php?error=updateFail");
            exit();
        }
        mysqli_stmt_bind_param($stmt, "sss", $name, $mail, $email);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        
        if ($role != "Manager") {
            $_SESSION["name"] = $name;
            $_SESSION["email"] = $mail;
        }
        
        header("location: ../profile.php?error=ok");
    }

    function  getBalance($conn , $email)
    {
        $query = "SELECT balance FROM user WHERE email = ?;";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $query)) {
            header("location: ../index.php?error=getBalancefail");
            exit();
        }
        mysqli_stmt_bind_param($stmt, "s", $email);
        mysqli_stmt_execute($stmt);
        $resultData = mysqli_stmt_get_result($stmt);

        if ($row = mysqli_fetch_assoc($resultData)) {
            return $row;
        } else {
            return false;
        }
        return $resultData;
        
        mysqli_stmt_close($stmt); 
    }

    function  deleteAccount($conn, $email)
    {
        $query = "DELETE user WHERE email = ?;";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $query)) {
            header("location: ../index.php?error=delAccountFail");
            exit();
        }
        mysqli_stmt_bind_param($stmt, "s", $email);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        
        header("location: ../index.php?error=ok");
        
    }

