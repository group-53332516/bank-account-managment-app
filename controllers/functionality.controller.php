<?php

    require_once  '../model/functionality.model.php';

    function update($conn)
    {       
        if (isset($_POST["submit"])) {
            $email = $_POST["email"];
            $name = $_POST["name"]
            $password = $_POST["password"];
            if ($_SESSION['role'] != "manager") {
                $mail = $_SESSION["email"];
            }else {
                $mail = $_SESSION['mail']
            }

            updateAnAccount($conn, $name, $mail, $email, $_SESSION['role']);

        }else {
            header("location: ../index.php");
        }
    }

    
    function delete($conn)
    {
        if (isset($_POST["submit"])) {
            $email = $_POST["email"];
            
            deleteAccount($conn, $email)

        }else {
            header("location: ../index.php");
        }
    }

    function getbalance($conn)
    {
        if (isset($_POST["submit"])) {
            $email = $_SESSION["email"];
            
            $balance = getBalance($conn , $email)
            return $balance;

        }else {
            header("location: ../index.php");
        }
    }
