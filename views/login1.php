<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MY BANKING APP - Login</title>
    <style>
        body{
            font-family: Arial,
            Sans-serif;
            background: #f1f1f1;
            margin: 0;
            padding: 20px;
        }
        h1{
            color: #333;
        }
        .login-form{
            background: #fff;
            padding: 20px;
            border-raduis: 4px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        .login-form label {
                display:block;
                margin-bottom: 10px;
            }
        .login-form input[type="text"],
        .login-form input[type="password"]
        {
            width: 100%;
            padding: 8px;
            border: 1px solid #ccc;
            border-raduis: 4px;
        }
        .login-form input[type="submit"]{
            background: #4CAF50;
            color:#fff;
            padding: 8px 16px;
            border:none;
            border-raduis:4px;
            cursor:ponter;
        }
    </style>
</head>

<body>
    <h1>LOGIN</h1>
    <form action="login.php"
    method="POST">
    <label for="username">Username:</label>
    <input type="text" name="usernme" required><br>
    <label for="password">Password:</label> required><br>
    <input type="password" name="Password" required><br>
    <input type="submit" value="Login">
</form>
</body>
</html>