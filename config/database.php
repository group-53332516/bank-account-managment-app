<?php
//Database connection configuration
$host = '';
$username = '';
$password = '';
$database = '';

//Establish the database connection
$conn = new mysqli($host,$username,$password,$database);
if ($conn->connect_errno) {
    die("Failed to connect to the database: " . $conn->connect_error);
}
?>